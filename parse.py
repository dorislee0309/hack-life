import os
import sched
import time
import numpy as np
DEBUG=False
PDT_TO_UTC = 7 #Converting from Pacific Daylight time to UTC 
#Repeating this procedure every hour
# os.system("wget https://www.google.com/calendar/feeds/dorislee%40berkeley.edu/private-3f5540f361ab70c7917b65153e3d0830/basic")

def parse():
	# if (DEBUG):
	# os.system("wget https://www.google.com/calendar/ical/dorislee%40berkeley.edu/private-3f5540f361ab70c7917b65153e3d0830/basic.ics")
	os.system("gback/gback/gback.py  --export SF ")
	f = open("SF.ical",'r')
	lst = f.readlines()
	#lst = f.readlines()[:3000] # This slices the data to about the past 10 days. Don't need to conduct analysis on my Google Calendar schedule back when I was in high school...
	# Implement time code here, auto downlaod data every 5 hours and redo analysis.
	 
	#For each event we have [<EVENT_NAME>,<DTSTART>,<DTEND>,<DURATION_IN_SECONDS>]
	event_lst = []
	temp = [0,0,0,0]
	for line in lst:
	#Filling up the first 3 elements of event list
	    if ("SUMMARY" in line):
	        temp[0]=line.split(":")[1]
	    if ("DTSTART" in line):
	        # print line.split(":")
	        temp[1]=line.split(":")[1]
	    if ("DTEND" in line):
	        temp[2]=line.split(":")[1]
	        event_lst.append(temp)
	        temp = [0,0,0,0]
	#The zero second events are day reminders that I set for the far future, they are non-countable events
	#Duration Calculation (Actually UTC Offset doesn't matter since constant & offset so interval still remain the same)
	for event in event_lst:
	    duration = 0
	    if (len(event[1])>=16):
	        tup1 = (int(event[1][:4]),int(event[1][4:6]),int(event[1][6:8]),int(event[1][9:11]),int(event[1][11:13]),0,0,0,0)
	        #print tup
	    if (len(event[2])>=16):
	        tup2 = (int(event[2][:4]),int(event[2][4:6]),int(event[2][6:8]),int(event[2][9:11]),int(event[2][11:13]),0,0,0,0)
	    start = time.mktime(tup1)
	    end = time.mktime(tup2)
	    duration = end - start
	    event[3]=duration
	#Day events don't count in our analysis (They serve only as future reminders for myself)
	for event in event_lst:
	    if (len(event[1])==10):
	        #print 'Day Events removed'
	        event_lst.remove(event)
	return  event_lst
