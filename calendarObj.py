class Calendar(object):
    '''
    Each calendar object represents one google calendar 
    '''
    def __init__(self,name,reghrs):
        self.name = name
        self.reghrs = reghrs
        self.projhrs = reghrs #Projected number of hours to work on things on this calendar per week
        self.hrs = 0.0 #actual hours spent on this calendar events this week
    def set_projhrs(self, new_hrs):
        print "Changing projected hours"
        choice = raw_input("Do you want to keep spending {} hours on {} as usual?".format(self.reghrs, self.name))
        if (choice == "N" or "no" or "n"):
            h = raw_input("How much time do you plan on {} this week?".format(self.name))
            self.projhrs=h
    def __str__(self):
        return  "<Calendar: " +self.name + "; Default Hours : "+str(self.reghrs)+ ";Projected Hours : "+str(self.projhrs)+">"


if __name__ == "__main__":
    # Test Method 
    SF = Calendar("SF",15)
    print SF
    SF.set_projhrs(20)
    print SF.projhrs

        

