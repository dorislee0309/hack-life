from gback import GCalSession
import datetime
from calendarObj import Calendar
class CalendarList(object):
	def __init__(self):
		self.lst=self._init_list()
	def _init_list(self):
		session = GCalSession("gback.oauth")
		lst = []
		for calnames in session.names:
			# record = open("reghrs_record.txt",'a')
			# record.write("{} {}".format(name,reghrs))
			# print calnames
			start=False
			print calnames
			with open("reghrs_record.txt",'r') as f:
				for line in f:
					a = str(line)[0]
					if a[0] =="@": 
						start=True
						continue
					if (start):
						name = str(line).split()[0]
						hr = str(line).split()[1]
						# print name , hr		
						print "name: "+name
						if (calnames==name)	:
							cal = Calendar(name,float(hr))
							print cal
						# print "cal reg hrs: " +  str(cal.reghrs)
						# print cal 
			lst.append(cal)
		# print "lst 0 :" 
		# print lst[0]
		return lst
	# def add_new_Calendar(self,name, reghrs):
	def __str__(self):
		string = "<CalendarList: "
		for i in self.lst:
			string+=  i.name+ ","
		return  string+">"
	def detailed_print_week(self):
		print "          | Projected Hours this week| Already Done"
		for i in self.lst:
			# print i
			print i.name +" : " + str(i.projhrs) +"|"+ str(i.hrs)
	def detailed_print_day(self):
		print "	|	Projected Hours today	|	Already Done"
		for i in self.lst:
			print i.name +":	" + str(i.projhrs/7. ) +"	|	"+ str(i.hrs/7.)
if __name__ == "__main__":
	# Test Method 
	calLst = CalendarList()
	print calLst
	calLst.detailed_print_week()
	# calLst.detailed_print_day()

